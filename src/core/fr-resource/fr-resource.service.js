"use strict";

var angular = require('angular');

angular
    .module('fr-resource')
    .factory('frResource', frResource);

/* @ngInject */
function frResource($resource) {
    return function( url, paramDefaults, actions, options ) {
        var defaults = {
            update: { method: 'PUT', isArray: false },
            create: { method: 'POST' }
        };

        actions = angular.extend( defaults, actions );

        var resource = $resource( url, paramDefaults, actions, options );

        resource.prototype.$save = function() {
            if ( !this.id ) {
                return this.$create.apply(this, arguments);
            } else {
                return this.$update.apply(this, arguments);
            }
        };

        return resource;
    };
}
