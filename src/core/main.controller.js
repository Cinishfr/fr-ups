"use strict";

var angular = require('angular');

var logoUrl = require('./logo.svg');

angular
    .module('app')
    .controller('MainController', MainController);

/* @ngInject */
function MainController($state, AuthenticationService) {
    var mvm = this;
    mvm.logout = logout;
    mvm.logoUrl = logoUrl;

    function logout() {
        AuthenticationService.logout().then(function () {
            $state.go('login');
        });
    }
}
