"use strict";
var angular = require('angular');

var mainUrl = require('./main.html');
var homeUrl = require('./home.html');
var error404Url = require('./404.html');

require('./main.controller');

angular
    .module('app')
    .config(config);

/* @ngInject */
function config($locationProvider, $httpProvider, $transitionsProvider, $stateProvider, $urlRouterProvider) {
    console.log("app.config invoked..");
    $locationProvider.html5Mode(true);
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    // $transitionsProvider.onBefore({ to: 'main.**' }, function (transition) {
    //     var authService = transition.injector().get('AuthenticationService');
    //     return authService.isAuthenticated().then(function (isAuthenticated) {
    //         if (!isAuthenticated) {
    //             // TODO: add alert service addition of no access error
    //             return transition.router.stateService.go('login');
    //         }
    //     });
    // });

    $stateProvider.state({
        name: 'index',
        url: '/',
        redirectTo: 'main.home'
    })
    .state({
        name: 'main',
        url: '/app',
        templateUrl: mainUrl,
        redirectTo: 'main.home',
        controller: 'MainController',
        controllerAs: 'mvm'
    })
    .state({
        name: 'main.home',
        url: '/home',
        templateUrl: homeUrl
    })
    .state({
        name: 'error404',
        url: '/404',
        templateUrl: error404Url
    })
    .state({
        name: 'test',
        url: '/test',
        template: '<h1>Test</h1>'
    });

    $urlRouterProvider.otherwise('/404');
}
