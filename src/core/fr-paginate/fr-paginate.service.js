"use strict";

var angular = require('angular');

angular
    .module('fr-paginate')
    .factory('frPaginate', frPaginate);

/* @ngInject */
function frPaginate() {
    var service = {
        page: 1,
        limit: 100,
        totalPages: 1,
        paginate: paginate,
        calculateTotalPages: calculateTotalPages,
        goTo: goTo,
        next: next,
        previous: previous,
        first: first,
        last: last
    };

    return service;

    function paginate(query) {
        if (typeof query === 'undefined') query = {};
        query.page = service.page;
        query.limit = service.limit;
        return query;
    }

    function calculateTotalPages(resourceCount) {
        service.totalPages = Math.ceil(resourceCount / service.limit);
        return service.totalPages;
    }

    function goTo(page) {
        service.page = page;
    }

    function next() {
        service.page++;
    }

    function previous() {
        service.page--;
    }

    function first() {
        service.page = 1;
    }

    function last() {
        service.page = service.totalPages;
    }
}
