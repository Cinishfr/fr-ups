"use strict";

// require('/node_modules/bootstrap/less/bootstrap.less');
require('./app.less');

var angular = require('angular');
var angularResource = require('angular-resource');
var angularAnimate = require('angular-animate');
var angularTouch = require('angular-touch');
var angularSanitize = require('angular-sanitize');
require('angular-ui-router');

var angularUiBootstrap = require('angular-ui-bootstrap');

var frAlerts = require('./fr-alerts/fr-alerts.module');
var frPaginate = require('./fr-paginate/fr-paginate.module');
var frResource = require('./fr-resource/fr-resource.module');

module.exports = angular.module('app', [
    angularSanitize,
    angularResource,
    'ui.router',
    angularAnimate,
    angularTouch,
    angularUiBootstrap,
    frAlerts.name,
    frPaginate.name,
    frResource.name
]);

require('./app.config');
console.log("app.module invoked");
