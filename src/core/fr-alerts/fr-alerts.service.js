"use strict";

var angular = require('angular');

angular
    .module('fr-alerts')
    .factory('frAlerts', frAlerts);

/* @ngInject */
function frAlerts() {
    var service = {
        clearAll: clearAll,
        add: add,
        remove: remove,
        handleResourceError: handleResourceError,
        handleXHRError: handleXHRError,
        getErrorMessage: getErrorMessage,
        getAlerts: getAlerts,
        alerts: []
    };

    return service;

    function clearAll() {
        service.alerts = [];
    }

    function add(message, type) {
      if (typeof type === "undefined") {
            type = 'info';
        }
        service.alerts.push({type: type, message: message});
    }

    function remove(index) {
        service.alerts.splice(index, 1);
    }

    function handleResourceError(httpResponse) {
        console.error(httpResponse);
        var msg = 'Error' + getErrorMessage(httpResponse.data);
        add(msg, 'danger');
    }

    function handleXHRError(msg, data, status, headers, config) {
        console.error(msg, data, status, headers, config);
        if (status === 403) {
            msg = 'You do not have access to self function.';
            return add(msg, 'danger');
        }
        msg = 'Error' + getErrorMessage(data);
        add(msg, 'danger');
    }

    function getErrorMessage(data) {
        if (!!data.err) {
            return ': ' + data.err;
        } else if (!!data.error) {
            if (typeof data.error.message !== 'undefined') {
                return ': ' + data.error.message;
            } else if (typeof data.error === 'string') {
                return ': ' + data.error;
            }
        } else if (!!data.message) {
            return ': ' + data.message;
        }
        return '';
    }

    function getAlerts() {
        return service.alerts;
    }
}
