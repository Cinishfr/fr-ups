"use strict";

var angular = require('angular');

var frAlertsDisplayUrl = require('./fr-alerts-display.html');

angular
    .module('fr-alerts')
    .directive('frAlertsDisplay', frAlertsDisplay);

/* @ngInject */
function frAlertsDisplay() {
    var directive = {
        restrict: 'E',
        templateUrl: frAlertsDisplayUrl,
        scope: {},
        controller: FrAlertsDisplayController,
        controllerAs: 'vm',
        bindToController: true
    };

    return directive;
}

/* @ngInject */
function FrAlertsDisplayController(frAlerts) {
    var vm = this;
    vm.getAlerts = getAlerts;
    vm.remove = remove;

    function getAlerts() {
        return frAlerts.getAlerts();
    }

    function remove(index) {
        frAlerts.remove(index);
    }
}
