"use strict";
var angular = require('angular');
var appModule = require('./core/app.module');

angular.element(document).ready(function () {
    angular.bootstrap(document, [appModule.name], {
        // strictDi: true
    });
});
