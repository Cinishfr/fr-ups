"use strict";

const path = require('path');
const cfenv = require('cfenv');
const localVCAP = require("./local-vcap.json");

const appEnv = cfenv.getAppEnv({
    vcap: localVCAP
});

/******************
 * Express Server *
 ******************/

const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
let bodyParser = require('body-parser');
require('body-parser-xml')(bodyParser);
const basicAuth = require('express-basic-auth');
const users = require('./data/config/users.json');

// Configure express app
const app = express();

app.use(express.static('public'));

// app.use(bodyParser.xml({type:"application/xml"}));

app.use(bodyParser.json({
    limit: '2mb'
}));

app.use(bodyParser.urlencoded({
    extended: false
}));
//
// App routers
const apiRouter = require('./server/routes');

app.set('port', appEnv.port || 3000);

// app.use('/', basicAuth({
//     users: users
// }));

app.use('/', apiRouter);

// HTML5 Mode catch all route
app.all('/*', function(req, res, next) {
    console.log("Hit catch all route..");
    res.sendFile(__dirname + '/index.html');
});

// Post route middleware
app.use(logErrors);
app.use(clientErrorHandler);

http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on ' + appEnv.url);
});


// Middlewares

function logErrors(err, req, res, next) {
    console.error(err);
    next(err);
}

function clientErrorHandler(err, req, res, next) {
    if (res.statusCode === 200) {
        if (err.statusCode) {
            res.status(err.statusCode);
        } else {
            res.status(500);
        }
    }
    if (req.xhr || (req.get('Content-Type') === 'application/json')) {
        const response = {
            message: err.message
        };
        if (err.errors) {
            response.errors = err.errors;
        }
        res.json(response);
    } else {
        res.send(err.message);
    }
}
