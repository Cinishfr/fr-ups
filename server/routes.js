"use strict";
const express = require('express');

const upsRouter = require('./ups/routes');
const requestRouter = require('./upsreqres/routes');

const router = express.Router();

console.log("server routes..");

router.use('/ups', upsRouter);
router.use('/request', requestRouter);

module.exports = router;
