"use strict";

const express = require('express');

const upsController = require('./ups.controller');

const router = express.Router();

router.get('/track/:trackingNumber', upsController.track);
router.post('/ship', upsController.ship);
router.post('/callTag', upsController.callTag);

module.exports = router;
