"use strict";

const builder = require('xmlbuilder');
const extend = require('node.extend');
const https = require('https');
const xml2js = require('xml2js');
const parser = xml2js.Parser();
const fs = require('fs');
const testPath = '../../data/test';
const soap = require('soap');
const path = require('path');
require('request').debug = true;
const DOMParser = require('xmldom').DOMParser;
const domParser = new DOMParser();
const XMLSerializer = require('xmldom').XMLSerializer;
const xmlSerializer = new XMLSerializer();


class UPSService {
    constructor() {
    }

    static getHosts(){
        const hosts = {
            sandbox: 'wwwcie.ups.com',
            live: 'onlinetools.ups.com'
        };
        return hosts;
    }

    static getConnectionOptions() {
        const options =  {
            imperial: true, // for inches/lbs, false for metric cm/kgs
            currency: 'USD',
            environment: 'sandbox',
            access_key: '4D2959D5206324D9',
            username: 'cinish',
            password: 'Fresh1-2',
            accountNumber: '4E23E6',
            pretty: true,
            user_agent: 'uh-sem-blee, Co | typefoo',
            debug: true
        };

        return options;
    }

    static track(trackingNumber, collectResponseCallback) {
        const _resourcePath = '/ups.app/xml/Track';
        let track = this;
        let trackResponse = null;
        let trackError = null;

        const buildParamsForHttpsLibrary = function(hosts, options, body , resourcePath ) {
            var params = {
                host: hosts[options.environment],
                path: resourcePath ,
                method: 'POST',
                headers: {
                    'Content-Length': body.length,
                    'Content-Type': 'text/xml',
                    'User-Agent': options.user_agent
                }
            };
            return params;
        };

        const buildTrackingRequest = function (tracking_number, options) {
            if (!options) {
                options = {};
            }
            var root = builder.create('TrackRequest', {
                headless: true
            });
            var request = {
                'Request': {
                    'RequestAction': 'Track',
                    'RequestOption': options.latest === true ? '0' : '1',
                    'TransactionReference': {
                        'CustomerContext': options.transaction_id || ''
                    }
                }
            };

            request.TrackingNumber = tracking_number;
            if (options && options.extra_params && typeof options.extra_params === 'object') {
                request = extend(true, request, options.extra_params);
            }

            root.ele(request);
            const trackingRequest = root.end({
                pretty: options.pretty
            });
            return trackingRequest;
        };

        const buildAccessRequest = function (data, options) {
                var root = builder.create('AccessRequest', {
                    headless: true
                });
                root.att('xml:lang', 'en-US');
                root.ele('UserId', options.username);
                root.ele('Password', options.password);
                root.ele('AccessLicenseNumber', options.access_key);
                return root.end({
                    pretty: options.pretty
                });
        };

        const buildCompleteTrackingRequest = function (trackingNumber, options) {
            const _xmlDeclaration = '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>';
            let trackRequest = buildTrackingRequest(trackingNumber, options);
            let accessRequest = buildAccessRequest({}, options);

            var completeTrackingRequest = _xmlDeclaration + accessRequest  + trackRequest;
            return completeTrackingRequest;
        };

        const sendRequest = function (params, body, callback ){
            let req = https.request(params);

            req.write(body);
            req.on('error', function(e) {
                return callback(e, null);
            });
            req.on('response', function(res) {
                var responseData = '';

                res.on('data', function(data) {
                    responseData += data.toString();
                });

                res.on('end', function() {
                    if (params.filter && typeof params.filter === 'function') {
                        responseData = params.filter(responseData);
                    }
                    callback(null, responseData);
                });
            });
            req.end();
        };

        const options = this.getConnectionOptions();
        const hosts = this.getHosts();

        let trackRequest = buildTrackingRequest(trackingNumber,options);
        let accessRequest = buildAccessRequest({},  options);
        let completeTrackingRequest = buildCompleteTrackingRequest(trackingNumber,  options);
        let params = buildParamsForHttpsLibrary ( hosts, options, completeTrackingRequest , _resourcePath );

        sendRequest(params, completeTrackingRequest, collectResponseCallback);
    }

    static ship( shipRequest ,collectShipResponseCallback) {
        const _resourcePath = '/webservices/Ship'; //https://wwwcie.ups.com/webservices/Ship
        const _shipWsdl = '../../data/config/Ship.wsdl';

        const options = this.getConnectionOptions();
        const hosts = this.getHosts();

        const createSoapClient = function (processShipmentCallback) {
             soap.createClient(path.join(__dirname, _shipWsdl), {}, processShipmentCallback );
        };

        const getShipmentRequest = function () {
            return shipRequest;
        };

        const processShipmentCallback = function ( err , soapClient ) {

            const invokeSoapProcessShipment = function (  ) {
                const createSoapHeader = function () {
                    return `<upss:UPSSecurity xmlns:upss="http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0"  xmlns:envr="http://schemas.xmlsoap.org/soap/envelope/" xmlns:auth="http://www.ups.com/schema/xpci/1.0/auth" xmlns:common="http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0"  xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <upss:UsernameToken>
                    <upss:Username>${options.username}</upss:Username>
                    <upss:Password>${options.password}</upss:Password>
                    </upss:UsernameToken>
                    <upss:ServiceAccessToken>
                    <upss:AccessLicenseNumber>${options.access_key}</upss:AccessLicenseNumber>
                    </upss:ServiceAccessToken>
                    </upss:UPSSecurity>`;
                };

                const soapHeader = createSoapHeader();
                soapClient.addSoapHeader(soapHeader);

                const shipmentRequest = getShipmentRequest();

                soapClient.ProcessShipment(shipmentRequest, collectShipResponseCallback);
            };

            invokeSoapProcessShipment();
        };

       createSoapClient(processShipmentCallback);
}

    callTag() {
        console.log("UPSService callTag...");
    }
}

module.exports = UPSService;
