"use strict";

const UPSService = require('./ups.service');
const upsService = new UPSService();
const moment = require('moment');
const xml2js = require('xml2js');
const parser = new xml2js.Parser();

const xpath = require('xpath');
const DOMParser = require('xmldom').DOMParser;
const domParser = new DOMParser();
const XMLSerializer = require('xmldom').XMLSerializer;
const xmlSerializer = new XMLSerializer();
require('request').debug = true;

const pd = require('pretty-data').pd;
const fs = require('fs');

const ShipSuccessResponseParser = require('../upsreqres/shipsuccessparser.service');
const shipSuccessResponseParser = new ShipSuccessResponseParser();

const ZPLService = require('../zpl/zpl.service');
const zplService = new ZPLService();

module.exports = {
    track: track,
    ship: ship,
    callTag: callTag
};

function track(req, res, next) {
    const collectResponseCallback = function (error, response) {
        if (error) {
            const formattedError = pd.xml(error);
            res.send(formattedError);
        } else {
            const formattedResponse = pd.xml(response);
            res.send(formattedResponse);
        }
    };

    const trackingNumber = req.params.trackingNumber;
    upsService.constructor.track(trackingNumber, collectResponseCallback);
}

function ship(req, res, next) {
    const requestBody = req.body;
    const requestXml = requestBody.requestXml;

    parser.parseString(requestXml, function (err, result) {
        console.log("result", result);
        const resultRootSection = result.root;
        const _commonSection = {
            "common:RequestOption": "nonvalidate",
            "common:TransactionReference": {
                "common:CustomerContext": ""
            }
        };
        resultRootSection['common:Request'] = _commonSection;

        const collectShipResponseCallback = function (err, shipmentResponse, soapClient) {

            const responseWrapper = {};
            if (err) {
                console.log("error in soapClient.ProcessShipment", err);
                console.log("soapClient.lastRequest", soapClient.lastRequest);
                responseWrapper.error = pd.xml(err.body);
                res.send(responseWrapper);
            } else {
                const xmlBuilder = new xml2js.Builder();
                const shipmentResponseXml = xmlBuilder.buildObject(shipmentResponse);
                console.log("soapClient.lastRequest", soapClient.lastRequest);
                responseWrapper.upsResponse = pd.xml(shipmentResponseXml);
                const trackingNumber = shipSuccessResponseParser.constructor.getTrackingNumber(shipmentResponseXml);
                console.log("trackingNumber", trackingNumber);
                responseWrapper.trackingNumber = trackingNumber;
                const zpl = shipSuccessResponseParser.constructor.getZpl(shipmentResponseXml);
                console.log("zpl", zpl);
                if (zpl && zpl != 'No ZPL Content') {
                    const saveFileCallBack = function (err, resp, body) {
                        if (err) {
                            return console.log(err);
                        }
                        fs.writeFileSync( __dirname + '/../../public/upsLabel.pdf', body);
                        responseWrapper.zpl = zpl;
                        res.send(responseWrapper);
                    };

                    zplService.constructor.saveAsPdf(zpl, saveFileCallBack);
                } else {
                    res.send(responseWrapper);
                }
            }
        };

        upsService.constructor.ship(resultRootSection, collectShipResponseCallback);
    });
}

function callTag(req, res, next) {
    upsService.callTag();
    res.send(moment().format() + "::callTag");
}
