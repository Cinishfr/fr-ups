"use strict";

const fs = require('fs');
const request = require('request');

class ZPLService {
    constructor() {}

    static getLabelaryUrl() {
        return 'http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/';
    }

    static saveAsPdf(zpl, callback, filename =  __dirname + '/../../public/upsLabel.pdf') {
        var options = {
            encoding: null,
            formData: {
                file: zpl
            },
            headers: {
                'Accept': 'application/pdf'
            },
            url: this.getLabelaryUrl() // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
        };

        request.post(options, callback);
    }

    static saveAsPng(zpl, callback, filename =  __dirname + '/../../public/upsLabel.png') {
        var options = {
            encoding: null,
            formData: {
                file: zpl
            },
            url: this.getLabelaryUrl() // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
        };

        request.post(options, function(err, resp, body) {
            if (err) {
                return console.log(err);
            }
            fs.writeFileSync(filename, body);
        });
    }


}

module.exports = ZPLService;
