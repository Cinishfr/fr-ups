"use strict";

const zplservice = require('./zpl.service');
const testPath = '../../data/test/';

const fs = require('fs');

const zplUpsLabel = fs.readFileSync(testPath + "/upsLabel.zpl", "utf8");

zplservice.saveAsPng(zplUpsLabel);
zplservice.saveAsPdf(zplUpsLabel);
