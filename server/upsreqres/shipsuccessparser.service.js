"use strict";

const XmlParserService = require('./xmlparser.service');
const zplService = require('../zpl/zpl.service');
const base64 = require('base-64');
const fs = require('fs');
const testPath = '../../data/test';

class ShipSuccessResponseParser extends XmlParserService {
    constructor() {
        super();
    }

    static getZpl(xml) {
        const _zplxpath = "root/ShipmentResults/PackageResults/ShippingLabel/GraphicImage";
        const zplTagContent = this.parse(xml, _zplxpath);
        if(!zplTagContent){
            return 'No ZPL Content';
        }
        const zpl = Buffer.from(zplTagContent, 'base64').toString('ascii');
        return zpl;
    }

    static getTrackingNumber(xml) {
        console.log("xml", xml);
        const _trackingnumberxpath = "root/ShipmentResults/PackageResults/TrackingNumber";
        let trackingNumber = this.parse(xml, _trackingnumberxpath);
        if(!trackingNumber){
            return 'No tracking number';
        }
        return trackingNumber;
    }

    static getTrackingEvents() {

    }

    static getZplAsPdf(xml) {
        const zpl = this.getZpl(xml);
        zplService.saveAsPdf(zpl);
    }
}

module.exports = ShipSuccessResponseParser;
