"use strict";

const xpath = require('xpath');
const DOMParser = require('xmldom').DOMParser;
const domParser = new DOMParser();

class XmlParserService {
    constructor() {}

    static parse(xml, xpathExpression) {
        const node = this.getNode(xml, xpathExpression);
        if(node){
            const data = node.firstChild.data;
            return data;            
        }
    }

    static getNode(xml, xpathExpression) {
        const doc = this.getDoc(xml);
        const nsselect = xpath.useNamespaces({
            // "soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
            // "ship": "http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0",
            // "err": "http://www.ups.com/XMLSchema/XOLTWS/Error/v1.1"
        });

        const nodes = nsselect(xpathExpression, doc);
        return nodes[0];
    }

    static getDoc(xml) {
        return domParser.parseFromString(xml);
    }
}

module.exports = XmlParserService;
