"use strict";

const XmlParserService = require('./xmlparser.service');
const XMLSerializer = require('xmldom').XMLSerializer;
const xmlSerializer = new XMLSerializer();
const xpath = require('xpath');
const DOMParser = require('xmldom').DOMParser;
const domParser = new DOMParser();
const fs = require('fs');
const path = require('path');
const testPath = '../../data/test';


class CorruptRequestService extends XmlParserService {
    static _getShipperAddressXPath() {
        return "root/Shipment/Shipper/Address";
        // return "ship:ShipmentRequest/ship:Shipment/ship:Shipper/ship:Address";
        // return "Envelope/Body/ship:ShipmentRequest/ship:Shipment/ship:Shipper/ship:Address";
    }

    static _getShipmentXPath(){
        return "root/Shipment";
    }

    static _getPackageXPath(){
        return "root/Shipment/Package";
    }

    static corruptShipperSectionTag(tagName, invalidValue = 'INVALID' + tagName) {
        const xml = fs.readFileSync(path.join(__dirname,testPath) + "/shiprequest.xml", "utf8");
        const _tagNames = ['StateProvinceCode', 'PostalCode', 'CountryCode' ];
        if(_tagNames.indexOf(tagName) === -1){
            console.log("tag not matched:default xml\n", xml);
            return xml;
        }

        let doc = domParser.parseFromString(xml);
        // const nsselect = xpath.useNamespaces({
        //     "soapenv": "http://schemas.xmlsoap.org/soap/envelope/",
        //     "ship": "http://www.ups.com/XMLSchema/XOLTWS/Ship/v1.0",
        //     "err": "http://www.ups.com/XMLSchema/XOLTWS/Error/v1.1"
        // });
        //
        const _shippersectiontagxpath = this._getShipperAddressXPath() + "/" + tagName;
        console.log("_shippersectiontagxpath", _shippersectiontagxpath);
        // const _shippersectiontagxpath = this._getShipperAddressXPath() + "/ship:" + tagName;

        // const nodes = nsselect(_shippersectiontagxpath, doc);

        const nodes = xpath.select(_shippersectiontagxpath, doc);
        let stateNode = nodes[0];
        console.log("stateNode",stateNode);
        stateNode.textContent = invalidValue;
        const badRequest = xmlSerializer.serializeToString(doc);
        console.log("corruptShipperSectionTag badRequest\n", badRequest);
        return badRequest;
    }

    static corruptShipperPostalCode() {
        return this.corruptShipperSectionTag("PostalCode");
    }

    static corruptShipperCountryCode() {
        return this.corruptShipperSectionTag("CountryCode");
    }

    static addShipperRelease(){
        const xml = fs.readFileSync(path.join(__dirname,testPath) + "/shiprequest.xml", "utf8");
        let doc = domParser.parseFromString(xml);
        const _packageXpath =  this._getPackageXPath();
        const nodes = xpath.select(_packageXpath, doc);
        const packageNode = nodes[0];
        const packageServiceOptionsNode = doc.createElement('PackageServiceOptions');
        const shipperReleaseIndicatorNode = doc.createElement('ShipperReleaseIndicator');
        packageServiceOptionsNode.appendChild(shipperReleaseIndicatorNode).appendChild(doc.createTextNode("1"));
        packageNode.appendChild(packageServiceOptionsNode);
        const requestWithShipperRelease = xmlSerializer.serializeToString(doc);
        return requestWithShipperRelease;
    }

    static addReturnServiceRS1(){
        const _RS1Code = '3';

        const xml = fs.readFileSync(path.join(__dirname,testPath) + "/shiprequest.xml", "utf8");
        let doc = domParser.parseFromString(xml);
        const _shipmentXpath =  this._getShipmentXPath();
        const nodes = xpath.select(_shipmentXpath, doc);
        const shipmentNode = nodes[0];
        const returnServiceNode = doc.createElement('ReturnService');
        const descriptionNode = doc.createElement('Description');
        const codeNode = doc.createElement('Code');
        returnServiceNode.appendChild(descriptionNode).appendChild(doc.createTextNode('UPS Return Service 1-Attempt (RS1)'));
        returnServiceNode.appendChild(codeNode).appendChild(doc.createTextNode(_RS1Code));
        shipmentNode.appendChild(returnServiceNode);
        const requestWithReturnService = xmlSerializer.serializeToString(doc);
        return requestWithReturnService;
    }

    static addReturnServiceRS3(){
        const _RS3Code = '5';

        const xml = fs.readFileSync(path.join(__dirname,testPath) + "/shiprequest.xml", "utf8");
        let doc = domParser.parseFromString(xml);
        const _shipmentXpath =  this._getShipmentXPath();
        const nodes = xpath.select(_shipmentXpath, doc);
        const shipmentNode = nodes[0];
        const returnServiceNode = doc.createElement('ReturnService');
        const descriptionNode = doc.createElement('Description');
        const codeNode = doc.createElement('Code');
        returnServiceNode.appendChild(descriptionNode).appendChild(doc.createTextNode('UPS Return Service 3-Attempt (RS3)'));
        returnServiceNode.appendChild(codeNode).appendChild(doc.createTextNode(_RS3Code));
        shipmentNode.appendChild(returnServiceNode);
        const requestWithReturnService = xmlSerializer.serializeToString(doc);
        return requestWithReturnService;
    }

}

module.exports = CorruptRequestService;
