"use strict";

const shipsuccessparser = require('./shipsuccessparser.service');
const errorparser = require('./errorparser.service');
const corruptrequestservice = require('./corruptrequest.service');
const base64 = require('base-64');
const fs = require('fs');
const testPath = '../../data/test';

const xml = fs.readFileSync(testPath + "/shipresponse.xml", "utf8");
// const xml = fs.readFileSync(testPath + "/shipresponse-smallerimagecontent.xml", "utf8");
// shipsuccessparser.getZplAsPdf(xml);
// const zpl = shipsuccessparser.getZpl(xml);
// console.log("zpl", zpl);

shipsuccessparser.getGraphicImage(xml);
//
// const trackingNumber = shipsuccessparser.getTrackingNumber(xml);
// console.log("trackingNumber", trackingNumber);
//
// const errorxml = fs.readFileSync(testPath + "/error.xml", "utf8");
// const errorDescription = errorparser.getErrorDescription(errorxml);
// console.log("errorDescription", errorDescription);
//
// const requestxml = fs.readFileSync(testPath + "/shiprequest.xml", "utf8");
// const corruptedRequest = corruptrequestservice.corruptShipperCountryCode(requestxml, "WM");
// console.log("corruptedRequest ", corruptedRequest);
