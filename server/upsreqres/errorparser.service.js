"use strict";

const XmlParserService = require('./xmlparser.service.js');

class ErrorParser extends XmlParserService {
    constructor() {}

    static getErrorDescription(xml) {
        const _errorxpath = "soapenv:Envelope/soapenv:Body/soapenv:Fault/detail/err:Errors/err:ErrorDetail/err:PrimaryErrorCode/err:Description";
        return this.parse(xml, _errorxpath);
    }
}

module.exports = ErrorParser;
