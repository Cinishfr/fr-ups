"use strict";

const fs = require('fs');
const xml2js = require('xml2js');

const CorruptRequestService = require('./corruptrequest.service');
const corruptRequestService = new CorruptRequestService();
const testPath = '../../data/test';
const pd = require('pretty-data').pd;


const corruptRequest = function (req, res, next) {
    console.log("corruptRequest invoked..");
    const _fieldFunctions = {
        "PostalCode": corruptRequestService.constructor.corruptShipperPostalCode,
        "CountryCode": corruptRequestService.constructor.corruptShipperCountryCode
    };

    // const requestBody = req.body;
    // console.log("requestBody", requestBody);
    // const xmlBuilder = new xml2js.Builder();
    // const requestXml = xmlBuilder.buildObject(requestBody);
    // console.log("requestXml",requestXml);

    const fieldName = req.params.fieldName;

    let finalXml;
    switch (fieldName) {
        case 'ShipperRelease':
            finalXml = CorruptRequestService.addShipperRelease();
            break;
        case 'RS1':
            finalXml = CorruptRequestService.addReturnServiceRS1();
            break;
        case 'RS3':
            finalXml = CorruptRequestService.addReturnServiceRS3();
            break;
        case "PostalCode":
            finalXml = CorruptRequestService.corruptShipperPostalCode();
            break;
        case "CountryCode":
            finalXml = CorruptRequestService.corruptShipperCountryCode();
            break;
        default:
            console.log("fieldName", fieldName);
            finalXml = CorruptRequestService.corruptShipperSectionTag(fieldName);
    }
    const prettyXml = pd.xml(finalXml);
    res.send(prettyXml);

};

module.exports = {
    corruptRequest: corruptRequest
};
