"use strict";

const express = require('express');

const requestController = require('./request.controller');

const router = express.Router();

router.get('/:fieldName', requestController.corruptRequest);

module.exports = router;
