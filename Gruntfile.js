"use strict";
module.exports = function(grunt) {
    var pkg = require('./package.json');
    var path = require('path');
    var ext = function (file) {
        return path.extname(file).slice(1);
    };
    var mochaTestReporter = process.env.NODE_ENV === 'test' ? 'mocha-junit-reporter' : 'spec';
    var webpack = require("webpack");
    var webpackConfig = require('./webpack.config');
    var webpackDevURL = 'webpack.fr-ups.dev';
    var uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    });
    var ExtractTextPlugin = require("extract-text-webpack-plugin");

    var cfenv = require('cfenv');
    var localVCAP = require("./local-vcap.json");
    var appEnv = cfenv.getAppEnv({vcap: localVCAP});
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        notify: {
            server: {
                options: {
                   message: 'Server is ready!'
                }
            },
            tests: {
                options: {
                   message: 'Tests passed!'
                }
            }
        },
        concurrent: {
            tests: {
                tasks: [ 'watch:tests', 'watch:karma' ],
                options: {
                    logConcurrentOutput: true
                }
            },
            devServer: {
                tasks: [ 'webapp', 'watch:karma', 'watch:app' ],
                options: {
                    logConcurrentOutput: true
                }
            },
            devServerLight: {
                tasks: [ 'watch:appLight' ],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        watch: {
            options: {
                maxListeners: 20
            },
            tests: {
                files: ['**/*.js', '!src/**', '!node_modules/**'],
                tasks: ['mochaTest', 'jshint', 'notify:tests'],
                options: {
                    spawn: false,
                    atBegin: true
                },
            },
            karma: {
                files: ['src/**/*.js'],
                tasks: ['karma', 'jshint:karma', 'notify:tests'],
                options: {
                    spawn: false,
                    atBegin: true
                },
            },
            app: {
                files: ['**/*.js', '!node_modules/**', '!src/**', '!public/**'],
                tasks: ['mochaTest', 'jshint:app', 'express:dev', 'notify:server'],
                options: {
                    spawn: false,
                    atBegin: true
                },
            },
            appLight: {
                files: ['**/*.js', '!node_modules/**', '!src/**', '!public/**'],
                tasks: ['express:dev', 'notify:server'],
                options: {
                    spawn: false,
                    atBegin: true
                },
            }
        },
        jshint: {
            options: {
                reporter: require('jshint-stylish'),
                jshintrc: true
            },
            app: ['**/*.js', '!node_modules/**', '!public/**', '!src/**'],
            webApp: ['src/**/*.js']
        },
        mochaTest: {
            options: {
                reporter: mochaTestReporter,
                clearRequireCache: true
            },
            unitTests: {
                src: ['**/*.spec.js', '!src/**', '!node_modules/**']
            },
            integrationTests: {
                src: ['**/*.integration-spec.js', '!src/**', '!node_modules/**']
            }
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        },
        express: {
            dev: {
                options: {
                    script: 'app.js',
                    output: 'Express server listening on',
                    port: appEnv.port || 3000
                }
            }
        },
        localhosts: {
            set : {
                options: {
                    rules: [{
                        ip: '127.0.0.1',
                        hostname: webpackDevURL,
                        type: 'set'
                    }]
                }
            }
        },
        webpack: {
            app: webpackConfig
        },
        "webpack-dev-server": {
            options: {
                host: webpackDevURL,
                inline: true,
                // hot: true,
                historyApiFallback: true,
                publicPath: '/dev/' + webpackConfig.output.publicPath,
                proxy: [
                    {
                        path: '/api',
                        target: 'http://localhost:' + (appEnv.port || 3000),
                        host: 'localhost'
                    }
                ],
                webpack: {
                    devtool: "eval"
                }
            },
            start: {
                port: 8081,
                contentBase: webpackConfig.context,
                webpack: {
                    context: webpackConfig.context,
                    entry: webpackConfig.entry,
                    output: {
                        filename: '[name].min.js',
                        path: webpackConfig.output.path,
                        publicPath: 'dev/' + webpackConfig.output.publicPath
                    },
                    module: webpackConfig.module,
                    resolve: webpackConfig.resolve,
                    plugins: [
                        new ExtractTextPlugin({
                            filename: '[name].css'
                        })
                    ]
                }
            }
        },
        injector: {
            options: {
                template: 'src/index.html',
                destFile: 'public/index.html',
                transform: function (filepath) {
                    var e = ext(filepath);
                    filepath = filepath.replace('/dev', '');
                    if (e === 'css') {
                        return '<link rel="stylesheet" href="' + filepath + '">';
                    } else if (e === 'js') {
                        return '<script src="' + filepath + '"></script>';
                    } else if (e === 'html') {
                        return '<link rel="import" href="' + filepath + '">';
                    }
                }
            },
            app: {
                src: ['public/app/app-'+pkg.version+'.min.js', 'public/app/app-'+pkg.version+'.css']
            }
        }
    });

    // Load the plugins.
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-injector');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-localhosts');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-webpack');

    // Development setup tasks
    grunt.registerTask('dev-setup', [
        'localhosts:set'
    ]);

    // Tasks
    grunt.registerTask('ciTestRun', ['mochaTest:unitTests']);
    grunt.registerTask('build', ['webpack', 'injector']);
    grunt.registerTask('tests', ['concurrent:tests']);
    grunt.registerTask('webapp', ['jshint:webApp', 'webpack-dev-server:start']);
    grunt.registerTask('serve', ['concurrent:devServer']);
    grunt.registerTask('serve-light', ['concurrent:devServerLight']);
};
