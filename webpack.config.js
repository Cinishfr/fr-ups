"use strict";
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var NgAnnotateLoader = require('ng-annotate-loader');
var path = require('path');
var pkg = require('./package.json');

module.exports = {
    entry: {
        app: path.resolve(__dirname, 'src', 'app.bootstrap.js')
    },
    context: path.join(__dirname, 'src'),
    output: {
        filename: '[name]-'+pkg.version+'.min.js',
        path: path.resolve(__dirname, 'public', 'app'),
        publicPath: 'public/app'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node\_modules/, loader: 'ng-annotate-loader' },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader!less-loader"
                })
            },
            { test: /\.html$/, loader: "ngtemplate-loader?relativeTo=" + (path.resolve(__dirname)) + "/!html-loader?conservativeCollapse" },
            { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' },
            { test: /\.(woff|woff2)/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
            {test: /\.ttf$/,  loader: "url-loader?limit=10000&mimetype=application/octet-stream" },
            {test: /\.eot$/,  loader: "file-loader" },
            {test: /\.svg$/,  loader: "url-loader?limit=10000&mimetype=image/svg+xml" }
        ]
    },
    resolve: {
        modules: [
            'node_modules'
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name]-'+pkg.version+'.css'
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true
        })
    ]
};
